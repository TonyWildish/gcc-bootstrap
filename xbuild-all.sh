#!/bin/bash -l

echo "This still sources env.sh in the sub-build-scripts!"
exit 0

. ./xenv.sh
[ -d $INSTALL_DIR ] || mkdir -p $INSTALL_DIR
set -ex

mkdir -p $PREFIX/include
cp -r sysroot/usr/include/linux $PREFIX/include
cp -r sysroot/usr/include/asm $PREFIX/include/asm
cp -r sysroot/usr/include/asm-generic $PREFIX/include/

./build-binutils-$vsn_binutils.sh
./build-gcc-$vsn_gcc.sh

./build-glibc-$vsn_glibc.sh

cd obj-gcc-$vsn_gcc
make -j $NPROC all-target-libgcc
make install-target-libgcc

cd ../obj-glibc-2.12.2
make -j $NPROCS
make install

cd ../obj-gcc-4.8.3/
make -j $NPROCS
make install
