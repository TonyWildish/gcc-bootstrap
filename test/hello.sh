#!/bin/bash

set -ex
pushd ..
. ./env.sh
popd

PREFIX=$BSCRATCH/Denovo/install
export PATH=${PREFIX}/bin:${PREFIX}/x86_64-pc-linux-gnu/bin
export PATH=${PATH}:/usr/local/bin
export PATH=${PATH}:/bin

echo PATH = $PATH
echo LD_LIBRARY_PATH = $LD_LIBRARY_PATH

export GCC=gcc
export GXX=g++

exe=hello

[ -f $exe ] && rm -f $exe
[ -f $exe.o ] && rm -f $exe.o

set -ex
$GCC $CFLAGS -c $exe.c -o $exe.o
$GCC $LFLAGS $exe.o -o $exe
./$exe
/usr/bin/ldd $exe
[ -f $exe ] && rm -f $exe
[ -f $exe.o ] && rm -f $exe.o

$GCC $CFLAGS $exe.c -o $exe
./$exe
/usr/bin/ldd $exe
[ -f $exe ] && rm -f $exe
[ -f $exe.o ] && rm -f $exe.o

$GXX $CFLAGS $exe.cpp -o $exe
./$exe
/usr/bin/ldd $exe
[ -f $exe ] && rm -f $exe
[ -f $exe.o ] && rm -f $exe.o

/usr/bin/which $GCC
/usr/bin/which $GXX
