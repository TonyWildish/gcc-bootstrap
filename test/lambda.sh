#!/bin/bash

set -e
pushd ..
. ./env.sh
popd

export PATH=${INSTALL_DIR}/bin
export PATH=${PATH}:/usr/local/bin
export PATH=${PATH}:/bin

exe=lambda

[ -f $exe ] && rm -f $exe
[ -f $exe.o ] && rm -f $exe.o

set -ex
g++ --std=c++14 $exe.cpp -o $exe
./$exe
/usr/bin/ldd $exe
[ -f $exe ] && rm -f $exe
[ -f $exe.o ] && rm -f $exe.o

/usr/bin/which g++
