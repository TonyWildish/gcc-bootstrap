#!/bin/bash

set -ex
for vsn in 2.{22,23,24,25,26,27,28,29}; do
  file=binutils-$vsn.tar.gz
  url=https://ftp.gnu.org/gnu/binutils/$file
  [ -f $file ] || wget -O $file $url
done

for vsn in 4.8.3 4.9.2 4.9.4 5.4.0 6.2.0 6.4.0 7.1.0 ; do
  file=gcc-$vsn.tar.gz
  url=http://mirrors.ocf.berkeley.edu/gnu/gcc/gcc-$vsn/$file
  [ -f $file ] || wget -O $file $url
done

for vsn in 2.12.2 2.13 2.14.1 2.{15,16.0,17,18,19,20,21,22,23,24,25}; do
  file=glibc-$vsn.tar.bz2
  url=https://ftp.gnu.org/gnu/glibc/$file
  [ -f $file ] || wget -O $file $url
done
