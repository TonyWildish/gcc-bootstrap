#!/bin/bash

export ROOT=`pwd`

export gcc_vsn=4.8.3
export binutils_vsn=2.28
export glibc_vsn=2.12.2

export gcc_dir=gcc-$gcc_vsn
export binutils_dir=binutils-$binutils_vsn
export glibc_dir=glibc-$glibc_vsn

export gcc_obj=obj-$gcc_dir
export binutils_obj=obj-$binutils_dir
export glibc_obj=obj-$glibc_dir

export PREFIX=$BSCRATCH/Denovo/install

set -ex
if [ ! -d $binutils_dir ]; then
  tar xf binutils-$binutils_vsn.tar.gz
fi

if [ ! -d $glibc_dir ]; then
  tar xf glibc-$glibc_vsn.tar.gz
fi

if [ ! -d $gcc_dir ]; then
  tar xf gcc-$gcc_vsn.tar.gz
  cd $gcc_dir
  ./contrib/download_prerequisites

  cd ..
fi

[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX
tar --strip-components=1 -C $PREFIX -xf $ROOT/sysroot.tar.bz2

cd $ROOT
[ -d $glibc_obj ] && rm -rf $glibc_obj
mkdir -p $glibc_obj
cd $glibc_obj
../$glibc_dir/configure \
  --prefix=$PREFIX \
  --disable-multilib \
  --disable-nls \
  --with-headers=$PREFIX/usr/include
make -j 64
mkdir -p $PREFIX/etc/
touch $PREFIX/etc/ld.so.conf
make install-bootstrap-headers=yes install-headers
make install

cd $ROOT
[ -d $binutils_obj ] && rm -rf $binutils_obj
mkdir -p $binutils_obj
cd $binutils_obj
../$binutils_dir/configure \
  --prefix=$PREFIX \
  --with-local-prefix=$PREFIX \
  --disable-multilib \
  --enable-libssp \
  --enable-threads \
  --with-sysroot=$PREFIX
make -j 64
make install

cd $ROOT
export AR_FOR_TARGET=$PREFIX/bin/ar
export AS_FOR_TARGET=$PREFIX/bin/as
export LD_FOR_TARGET=$PREFIX/bin/ld
export NM_FOR_TARGET=$PREFIX/bin/nm
export OBJDUMP_FOR_TARGET=$PREFIX/bin/objdump
export RANLIB_FOR_TARGET=$PREFIX/bin/ranlib
export READELF_FOR_TARGET=$PREFIX/bin/readelf
export STRIP_FOR_TARGET=$PREFIX/bin/strip
#export CC_FOR_TARGET=$PREFIX/bin/gcc
#export CXX_FOR_TARGET=$PREFIX/bin/g++
#export GCC_FOR_TARGET=$PREFIX/bin/gcc
#export GFORTRAN_FOR_TARGET=$PREFIX/bin/gfortran

[ -d $gcc_obj ] && rm -rf $gcc_obj
mkdir -p $gcc_obj
cd $gcc_obj

# Does this help with sysroot?
# ...seems not...
export LIBRARY_PATH=$PREFIX/lib64:$PREFIX/usr/lib64

../$gcc_dir/configure \
  --prefix=$PREFIX \
  --with-local-prefix=$PREFIX \
  --disable-multilib \
  --enable-languages=c,c++,fortran \
  --enable-libssp \
  --enable-threads \
  --enable-bootstrap \
  --with-sysroot=$PREFIX

make -j 64
make install
