#!/bin/bash

mkdir -p asdf/$$/
for d in install `ls -d binutils-* glibc* gcc-* | egrep -v 'tar|gz|tgz'`
do
  [ -d $d ] && mv $d asdf/$$/
done

rm -rf asdf/
