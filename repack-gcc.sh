#!/bin/bash

set -ex
for f in gcc*.tar.gz; do
  echo $f
  base=`echo $f | sed -e 's%.tar.gz%%'`
  echo $base
  target=${base}-full
  if [ ! -f $target.tar.bz2 ]; then
    tar xf $f
    ls -ld $base
    cd $base
    ./contrib/download_prerequisites
    cd ..
    tar cf $target.tar $base
    bzip2 -v $target.tar
    rm -rf $base
  fi
done
