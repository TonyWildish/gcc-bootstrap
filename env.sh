#
# Versions
export vsn_glibc=2.12.2
export vsn_binutils=2.28
export vsn_gcc=4.8.3

# Binutils
file_binutils=binutils-$vsn_binutils.tar.gz
dir_binutils=binutils-$vsn_binutils
objdir_binutils=obj-$dir_binutils
# GCC
file_gcc=gcc-$vsn_gcc.tar.gz
dir_gcc=gcc-$vsn_gcc
objdir_gcc=obj-$dir_gcc
# GLIBC
file_glibc=glibc-$vsn_glibc.tar.gz
dir_glibc=glibc-$vsn_glibc
objdir_glibc=obj-$dir_glibc

export ROOT=`pwd`

if [ "$BUILD" == "" ]; then
  export BUILD=base
fi
export PREFIX=$BSCRATCH/Denovo/install/$BUILD
echo BUILD=$BUILD
echo PREFIX=$PREFIX

export TARGET=x86_64-unknown-linux-gnu
export TARGET_PREFIX=$PREFIX/$TARGET
[ -d $PREFIX ] || mkdir -p $PREFIX

module purge
export NPROCS=`grep -c processor /proc/cpuinfo`

alias wget=/usr/bin/wget
alias make=/usr/bin/make

export LD_LIBRARY_PATH=$PREFIX/base/lib64:$PREFIX/base/lib
export PATH=/bin:/usr/local/bin:/usr/bin:$PREFIX/bin:$PREFIX/sbin
#export PATH=$PREFIX/bin:$PREFIX/sbin:/bin:/usr/local/bin:/usr/bin


# Check which build I'm doing, 'base' or 'final', set some flags
if [ "$BUILD" != "base" ]; then
  export PATH=$ROOT/install/base/bin:$ROOT/install/base/sbin
  export PATH=${PATH}:/bin
  export PATH=${PATH}:/usr/local/bin
  export PATH=${PATH}:$ROOT/xcompile/bin
  export PATH=${PATH}:/usr/bin
  export PATH=${PATH}:$PREFIX/bin:$PREFIX/sbin
  export LD_LIBRARY_PATH=$PREFIX/$BUILD/lib64:$PREFIX/$BUILD/lib:$LD_LIBRARY_PATH
fi
