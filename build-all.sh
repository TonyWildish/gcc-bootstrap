#!/bin/bash -l

. ./env.sh

echo "# ENVIRONMENT ========================================"
env | sort
echo "# ENVIRONMENT ========================================"

[ -d $PREFIX ] || mkdir -p $PREFIX
set -ex

cd $ROOT
mkdir -p $PREFIX/include
cp -r sysroot/usr/include/linux $PREFIX/include
cp -r sysroot/usr/include/asm $PREFIX/include/asm
cp -r sysroot/usr/include/asm-generic $PREFIX/include/

# Binutils
echo 1/7 binutils | tee $ROOT/stage
[ -d $objdir_binutils ] && rm -rf $objdir_binutils
mkdir -p $objdir_binutils

[ -f $file_binutils ] || wget --no-check-certificate -O $file_binutils \
    https://ftp.gnu.org/gnu/binutils/$file_binutils
[ -d $dir_binutils ] || tar zxf $file_binutils

cd $objdir_binutils
../$dir_binutils/configure \
  --prefix=$PREFIX \
  --target=$TARGET \
  --disable-multilib \
  --disable-nls

make -j $NPROCS
make install-strip

# GCC, first pass 
echo 2/7 gcc, first pass | tee -a $ROOT/stage
cd $ROOT
[ -d $objdir_gcc ] && rm -rf $objdir_gcc
mkdir -p $objdir_gcc

[ -f $file_gcc ] || wget --no-check-certificate -O $file_gcc \
  http://mirrors.ocf.berkeley.edu/gnu/gcc/gcc-$vsn_gcc/$file_gcc

if [ ! -d $dir_gcc ]; then
  mkdir $dir_gcc
  tar xf $file_gcc -C $dir_gcc --strip-components=1

  echo "Downloading prerequisites"
  cd $dir_gcc
  ./contrib/download_prerequisites
  cd $ROOT
fi
cd $objdir_gcc
../$dir_gcc/configure \
  --without-headers \
  --prefix=$PREFIX \
  --disable-multilib \
  --disable-nls \
  --enable-languages=c,c++

make -j $NPROCS all
make install
# make -j $NPROCS all-gcc
# make install-gcc
# # make -j $NPROC all-target-libgcc
# # make install-no-fixedincludes || echo "Ignore the error..."

# GLIBC, first pass
echo 3/7 glibc, first pass | tee -a $ROOT/stage
cd $ROOT
[ -d $objdir_glibc ] && rm -rf $objdir_glibc
mkdir $objdir_glibc

[ -f $file_glibc ] || wget --no-check-certificate -O $file_glibc \
    https://ftp.gnu.org/gnu/glibc/$file_glibc
[ -d $dir_glibc ] || tar zxf $file_glibc

cd $objdir_glibc
../$dir_glibc/configure \
  --prefix=$PREFIX \
  --disable-multilib \
  --disable-nls \
  --with-headers=$PREFIX/include

make -j $NPROCS
make install-bootstrap-headers=yes install-headers
install csu/crt1.o csu/crti.o csu/crtn.o $PREFIX/lib

gcc \
  -nostdlib \
  -nostartfiles\
  -shared \
  -x c /dev/null \
  -o $PREFIX/lib/libc.so
touch $PREFIX/include/gnu/stubs.h

# GCC, libgcc
echo 4/7 libgcc | tee -a $ROOT/stage
cd $ROOT
cd $objdir_gcc
make -j $NPROC all-target-libgcc
make install-target-libgcc

# GLIBC, everything else
echo 5/7 glibc, everything else | tee -a $ROOT/stage
cd $ROOT
cd $objdir_glibc
make -j $NPROCS
make install

# GCC, the rest...
echo 6/7 gcc, the rest | tee -a $ROOT/stage
cd $ROOT
cd $objdir_gcc
make -j $NPROCS
make install

# Game over :-)
echo "7/7 All done!" | tee -a $ROOT/stage
