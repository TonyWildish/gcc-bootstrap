#!/bin/bash

export ROOT=`pwd`

export gcc_vsn=4.8.3
export gcc_dir=gcc-$gcc_vsn
export binutils_vsn=2.28
export binutils_dir=binutils-$binutils_vsn

set -ex
if [ ! -d $binutils_dir ]; then
  tar xf binutils-$binutils_vsn.tar.gz
fi

if [ ! -d $gcc_dir ]; then
  tar xf gcc-$gcc_vsn.tar.gz
  cd $gcc_dir
  ./contrib/download_prerequisites

  cd ..
fi

export obj_gcc=obj-$gcc_dir
export obj_binutils=obj-$binutils_dir
export PATH=$BSCRATCH/Denovo/install/bin:/bin:/usr/local/bin:/usr/bin
export LD_LIBRARY_PATH=$BSCRATCH/Denovo/install/lib64:$BSCRATCH/Denovo/install/lib:$BSCRATCH/Denovo/install/usr/lib64

export PREFIX=$BSCRATCH/Denovo/final/
[ -d $PREFIX ] && rm -rf $PREFIX
mkdir -p $PREFIX
tar --strip-components=1 -C $PREFIX -xf $ROOT/sysroot.tar.bz2

[ -d $obj_binutils ] && rm -rf $obj_binutils
mkdir -p $obj_binutils
cd $obj_binutils
../$binutils_dir/configure \
  --prefix=$PREFIX \
  --with-local-prefix=$PREFIX \
  --disable-multilib \
  --enable-libssp \
  --enable-threads \
  --with-sysroot=$PREFIX
make -j 64
make install

cd $ROOT
export AR_FOR_TARGET=$PREFIX/bin/ar
export AS_FOR_TARGET=$PREFIX/bin/as
export LD_FOR_TARGET=$PREFIX/bin/ld
export NM_FOR_TARGET=$PREFIX/bin/nm
export OBJDUMP_FOR_TARGET=$PREFIX/bin/objdump
export RANLIB_FOR_TARGET=$PREFIX/bin/ranlib
export READELF_FOR_TARGET=$PREFIX/bin/readelf
export STRIP_FOR_TARGET=$PREFIX/bin/strip
#export CC_FOR_TARGET=$PREFIX/bin/gcc
#export CXX_FOR_TARGET=$PREFIX/bin/g++
#export GCC_FOR_TARGET=$PREFIX/bin/gcc
#export GFORTRAN_FOR_TARGET=$PREFIX/bin/gfortran

[ -d $obj_gcc ] && rm -rf $obj_gcc
mkdir -p $obj_gcc
cd $obj_gcc

# Does this help with sysroot?
# ...seems not...
export LIBRARY_PATH=$PREFIX/lib64:$PREFIX/usr/lib64
export LIBRARY_PATH=$BSCRATCH/Denovo/install/lib64:$BSCRATCH/Denovo/install/usr/lib64

../$gcc_dir/configure \
  --prefix=$PREFIX \
  --with-local-prefix=$PREFIX \
  --disable-multilib \
  --enable-languages=c,c++,fortran \
  --enable-libssp \
  --enable-threads \
  --enable-bootstrap \
  --with-sysroot=$PREFIX

make -j 64
make install
