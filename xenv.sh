export vsn_glibc=2.12.2
export vsn_binutils=2.28
export vsn_gcc=4.8.3

export NPROCS=`grep -c processor /proc/cpuinfo`

module purge
export ROOT=`pwd`
export INSTALL_DIR=$ROOT/install/final

export TARGET=x86_64-pc-linux-gnu
export PREFIX=$INSTALL_DIR
export TARGET_PREFIX=$PREFIX/$TARGET
[ -d $INSTALL_DIR ] || mkdir -p $INSTALL_DIR

export GLIBC_DIR=$INSTALL_DIR
export BINUTILS_DIR=$INSTALL_DIR
export GCC_DIR=$INSTALL_DIR

export LD_LIBRARY_PATH=$BINUTILS_DIR/lib:$GLIBC_DIR/lib

alias wget=/usr/bin/wget
alias make=/usr/bin/make

export PATH=$ROOT/install/base/bin:/bin:/usr/local/bin:/usr/bin
